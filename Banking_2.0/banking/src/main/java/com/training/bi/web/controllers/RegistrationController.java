package com.training.bi.web.controllers;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bi.bo.constants.Constants;
import com.training.bi.bo.services.UserManager;
import com.training.bi.dao.entitites.Account;

public class RegistrationController extends BasicController {
	private static final long serialVersionUID = 1L;
	private UserManager userService;

	public RegistrationController() {
		super();
		userService = new UserManager(this.factory);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(Constants.REGISTRATION_JSP).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//get all data from request
		String email = req.getParameter(Constants.EMAIL).trim();
		String fName = req.getParameter(Constants.F_NAME).trim();
		String sName = req.getParameter(Constants.S_NAME).trim();
		String country = req.getParameter(Constants.COUNTRY).trim();
		String password = req.getParameter(Constants.PASSWORD).trim();
		String rePassword = req.getParameter(Constants.REPEAT_PASSWORD).trim();

		//check for errors
		String errorMess = this.errorsCheck(email, fName, sName, country, password, rePassword);

		if (errorMess.length() > 0) {
			req.setAttribute(Constants.ERROR_REG, errorMess);
			req.getRequestDispatcher(Constants.REGISTRATION_JSP).forward(req, resp);
		} 
		
		else {
			try {
				//check for existing user with the same email
				if (userService.findUser(email) == null) {
					Account newAccount = new Account(fName, sName, country, email, password);
					userService.createUser(newAccount);
					
					//set created account into the session
					req.getSession().setAttribute(Constants.EMAIL, newAccount);
					resp.sendRedirect(req.getContextPath() + "/my_cards");
				} 
				
				else {
					errorMess = "This email is already used. Check your email address.";
					req.setAttribute(Constants.ERROR_REG, errorMess);
					req.getRequestDispatcher(Constants.REGISTRATION_JSP).forward(req, resp);
				}
			} 
			
			catch (Exception e) {
				req.getRequestDispatcher(Constants.ERROR_JSP).forward(req, resp);
			}
		}
	}

	//check all data
	private String errorsCheck(String email, String fName, String sName, String country, 
			String password, String rePassword) {
		String errorMess = "";

		if (email.isEmpty()) {
			errorMess += "Enter your email address. ";
		} 
		
		else {
			Pattern pattern = Pattern.compile(Constants.EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(email);

			if (!matcher.matches()) {
				errorMess += "Check your email address. ";
			}
		}

		if (fName.isEmpty()) {
			errorMess += "Enter your name. ";
		}

		if (sName.isEmpty()) {
			errorMess += "Enter your second name. ";
		}

		if (country.isEmpty()) {
			errorMess += "Enter your country. ";
		}

		if (password.isEmpty()) {
			errorMess += "Enter password. ";
		} 
		
		else if (password.length() < Constants.MIN_PASS_LENGHT) {
			errorMess += "Your password is shorter than " +Constants.MIN_PASS_LENGHT + " characters. ";
		}

		if (rePassword.isEmpty()) {
			errorMess += "Repeat your password. ";
		}

		if (password.compareTo(rePassword) != 0) {
			errorMess += "Check your passwords: password and repeat password. ";
		}

		return errorMess;
	}
}