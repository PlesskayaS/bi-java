package com.training.bi.dao.interfaces;

import java.util.List;
import com.training.bi.dao.entitites.Card;

//Interface for objects from Card class with CRUD orepations
public interface ICardDAO {
	void createCard(Card newCard) throws Exception;

	void deleteCard(Card card) throws Exception;

	void updateCard(Card card) throws Exception;
	
	void updateCards(List<Card> cards) throws Exception;

	Card getCard(long id) throws Exception;

	List<Card> getCardsOfAccountId(int accountId) throws Exception;

	List<Card> getAllCard() throws Exception;
}