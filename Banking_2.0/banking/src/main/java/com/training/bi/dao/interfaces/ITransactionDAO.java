package com.training.bi.dao.interfaces;

import java.util.List;
import com.training.bi.dao.entitites.Transaction;

//Interface for objects from Transaction class with CRUD orepations
public interface ITransactionDAO {
	void createTransaction(Transaction newTransaction) throws Exception;

	void deleteTransaction(Transaction transaction) throws Exception;

	void updateTransaction(Transaction transaction) throws Exception;

	Transaction getTransaction(long id) throws Exception;

	List<Transaction> getAllTransaction() throws Exception;
	
	List<Transaction> getCardTransactions(long cardId) throws Exception;
}