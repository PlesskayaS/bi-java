package com.training.bi.dao.entitites;

//class that contains all the fields of table Card
public class Card {
	private long id;
	private int idAccount;
	private double cash;

	public Card() {
	}

	public Card(int idAccount, double cash) {
		this();
		this.idAccount = idAccount;
		this.cash = cash;
	}

	public Card(long id, int idAccount, double cash) {
		this(idAccount, cash);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long newId) {
		id = newId;
	}

	public int getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(int newIdAccount) {
		idAccount = newIdAccount;
	}

	public double getCash() {
		return cash;
	}

	public void setCash(double newCash) {
		cash = newCash;
	}
}