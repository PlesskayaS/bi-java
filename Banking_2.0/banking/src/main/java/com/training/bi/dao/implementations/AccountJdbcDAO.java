package com.training.bi.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.training.bi.dao.ConnectionManager;
import com.training.bi.dao.entitites.Account;
import com.training.bi.dao.interfaces.IAccountDAO;

//Implementation for interface IAccountDAO for accessing table "ACCOUNTS" in DB
public class AccountJdbcDAO implements IAccountDAO {
	private ConnectionManager connectionManager;

	public AccountJdbcDAO(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public void createAccount(Account newAccount) throws Exception {
		// this array contains the name of the column in the
		// "ACCOUNTS" table that contain the auto-generated keys that should be returned.
		String generatedColumns[] = { "ACCOUNT_ID" };

		String insertAccount = "INSERT INTO ACCOUNTS (ACCOUNT_ID, FIRST_NAME, SECOND_NAME, COUNTRY, EMAIL, ACCOUNT_PASSWORD) VALUES (ACCOUNT_ID_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(insertAccount, generatedColumns)) {

			statement.setString(1, newAccount.getFName());
			statement.setString(2, newAccount.getSName());
			statement.setString(3, newAccount.getCountry());
			statement.setString(4, newAccount.getEmail());
			statement.setString(5, newAccount.getPassword());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Creating user failed, no rows affected.");
			}

			// getting auto-generated id
			ResultSet generatedKey = statement.getGeneratedKeys();

			while (generatedKey.next()) {
				int id = generatedKey.getInt(1);
				newAccount.setId(id); // setting auto-generated id in account
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void deleteAccount(Account account) throws Exception {
		String deleteAccount = "DELETE FROM ACCOUNTS WHERE ACCOUNT_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(deleteAccount)) {

			statement.setInt(1, account.getId());

			int deletedRows = statement.executeUpdate();

			if (deletedRows == 0) {
				throw new Exception("No one object was deleted!");
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void updateAccount(Account account) throws Exception {
		String updateAccount = "UPDATE ACCOUNTS SET FIRST_NAME = ?, SECOND_NAME = ?, COUNTRY = ?, EMAIL = ?, ACCOUNT_PASSWORD =? WHERE ACCOUNT_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(updateAccount)) {

			statement.setInt(6, account.getId());
			statement.setString(1, account.getFName());
			statement.setString(2, account.getSName());
			statement.setString(3, account.getCountry());
			statement.setString(4, account.getEmail());
			statement.setString(5, account.getPassword());

			int updatedRow = statement.executeUpdate();

			if (updatedRow == 0) {
				throw new Exception("No one row was updated!");
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Account getAccount(int accountId) throws Exception {
		String getAccount = "SELECT * FROM ACCOUNTS WHERE ACCOUNT_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getAccount)) {

			statement.setInt(1, accountId);

			ResultSet result = statement.executeQuery();

			Account account = null;

			while (result.next()) {
				String firstName = result.getString("FIRST_NAME");
				String secondName = result.getString("SECOND_NAME");
				String countryName = result.getString("COUNTRY");
				String email = result.getString("EMAIL");
				String password = result.getString("ACCOUNT_PASSWORD");

				account = new Account(accountId, firstName, secondName, countryName, email, password);
			}
			return account;
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Account getAccount(String email) throws Exception {
		String getAccount = "SELECT * FROM ACCOUNTS WHERE EMAIL = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getAccount)) {

			statement.setString(1, email);

			ResultSet result = statement.executeQuery();

			Account account = null;

			while (result.next()) {
				int accountId = result.getInt("ACCOUNT_ID");
				String firstName = result.getString("FIRST_NAME");
				String secondName = result.getString("SECOND_NAME");
				String countryName = result.getString("COUNTRY");
				String password = result.getString("ACCOUNT_PASSWORD");

				account = new Account(accountId, firstName, secondName, countryName, email, password);
			}
			return account;
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Account> getAllAccount() throws Exception {
		List<Account> allAccounts = new ArrayList<Account>();
		String getAllAccount = "SELECT * FROM ACCOUNTS";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getAllAccount);) {

			ResultSet result = statement.executeQuery();

			while (result.next()) {
				int accountId = result.getInt("ACCOUNT_ID");
				String firstName = result.getString("FIRST_NAME");
				String secondName = result.getString("SECOND_NAME");
				String countryName = result.getString("COUNTRY");
				String email = result.getString("EMAIL");
				String password = result.getString("ACCOUNT_PASSWORD");

				Account account = new Account(accountId, firstName, secondName, countryName, email, password);
				allAccounts.add(account);
			}

			return allAccounts;
		}

		catch (Exception e) {
			throw e;
		}
	}
}