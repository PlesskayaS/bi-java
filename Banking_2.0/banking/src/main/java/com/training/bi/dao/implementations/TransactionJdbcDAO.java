package com.training.bi.dao.implementations;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.training.bi.dao.ConnectionManager;
import com.training.bi.dao.entitites.Transaction;
import com.training.bi.dao.interfaces.ITransactionDAO;

//Implementation for interface ITransactionDAO for accessing table "TRANSACTIONS" in DB
public class TransactionJdbcDAO implements ITransactionDAO {

	private ConnectionManager connectionManager;

	public TransactionJdbcDAO(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public void createTransaction(Transaction newTransaction) throws Exception {
		// this array contains the name of the column in the
		// "TRANSACTIONS" table that contain the auto-generated keys that should be returned.
		String generatedColumns[] = { "TRANSACTION_ID" };
		String insertTransaction = "INSERT INTO TRANSACTIONS (TRANSACTION_ID, TRANSACTION_DATE, CREDIT_CARD_FROM, CREDIT_CARD_TO, SUM_AMOUNT) "
				+ "VALUES (CARD_ID_SEQ.NEXTVAL, TO_DATE(sysdate, 'yyyy/mm/dd hh24:mi:ss'),?,?,?)";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(insertTransaction, generatedColumns)) {

			statement.setLong(1, newTransaction.getCardFrom());
			statement.setLong(2, newTransaction.getCardTo());
			statement.setDouble(3, newTransaction.getCashSum());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Adding transation is failed, no rows affected.");
			}
			
			// getting auto-generated id
			ResultSet generatedKey = statement.getGeneratedKeys();

			while (generatedKey.next()) {
				int id = generatedKey.getInt(1);
				newTransaction.setId(id);// setting auto-generated id in transaction
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void deleteTransaction(Transaction transaction) throws Exception {
		String deleteTransaction = "DELETE FROM TRANSACTIONS WHERE TRANSACTION_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(deleteTransaction)) {

			statement.setLong(1, transaction.getId());

			int deletedRows = statement.executeUpdate();

			if (deletedRows == 0) {
				throw new Exception("No one object was deleted!");
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void updateTransaction(Transaction transaction) throws Exception {
		String updateTransaction = "UPDATE TRANSACTIONS SET TRANSACTION_DATE= ?, CREDIT_CARD_FROM = ?, CREDIT_CARD_TO= ?, SUM_AMOUNT= ? WHERE TRANSACTION_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(updateTransaction)) {
			statement.setDate(1, transaction.getDate());
			statement.setLong(2, transaction.getCardFrom());
			statement.setLong(3, transaction.getCardTo());
			statement.setDouble(4, transaction.getCashSum());
			statement.setLong(5, transaction.getId());

			int updatedRow = statement.executeUpdate();

			if (updatedRow == 0) {
				throw new Exception("No one row was updated!");
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Transaction getTransaction(long id) throws Exception {
		String getTransaction = "SELECT * FROM TRANSACTIONS WHERE TRANSACTION_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getTransaction)) {

			statement.setLong(1, id);

			ResultSet result = statement.executeQuery();
			
			Transaction transaction = null;
			
			while (result.next()) {
				long transactiontId = result.getLong("TRANSACTION_ID");
				Date transactionDate = result.getDate("TRANSACTION_DATE");
				long transactionFrom = result.getLong("CREDIT_CARD_FROM");
				long transactionTo = result.getLong("CREDIT_CARD_TO");
				double transactionSum = result.getDouble("SUM_AMOUNT");

				transaction = new Transaction(transactiontId, transactionDate, transactionFrom, transactionTo,
						transactionSum);
			}
			return transaction;
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Transaction> getCardTransactions(long cardId) throws Exception {
		String getTransaction = "SELECT * FROM TRANSACTIONS WHERE CREDIT_CARD_FROM = ?";
		List<Transaction> transactList = new ArrayList<Transaction>();

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getTransaction)) {

			statement.setLong(1, cardId);

			ResultSet result = statement.executeQuery();

			while (result.next()) {
				long transactiontId = result.getLong("TRANSACTION_ID");
				Date transactionDate = result.getDate("TRANSACTION_DATE");
				long transactionFrom = result.getLong("CREDIT_CARD_FROM");
				long transactionTo = result.getLong("CREDIT_CARD_TO");
				double transactionSum = result.getDouble("SUM_AMOUNT");

				Transaction transact = new Transaction(transactiontId, transactionDate, transactionFrom, transactionTo,
						transactionSum);
				transactList.add(transact);
			}
			return transactList;
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Transaction> getAllTransaction() throws Exception {
		List<Transaction> allTransaction = new ArrayList<Transaction>();
		String getAllTransaction = "SELECT * FROM TRANSACTIONS";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getAllTransaction)) {

			ResultSet result = statement.executeQuery();

			while (result.next()) {
				long transactiontId = result.getLong("TRANSACTION_ID");
				Date transactionDate = result.getDate("TRANSACTION_DATE");
				long transactionFrom = result.getLong("CREDIT_CARD_FROM");
				long transactionTo = result.getLong("CREDIT_CARD_TO");
				double transactionSum = result.getDouble("SUM_AMOUNT");

				Transaction transaction = new Transaction(transactiontId, transactionDate, transactionFrom,
						transactionTo, transactionSum);
				allTransaction.add(transaction);
			}

			return allTransaction;
		}

		catch (Exception e) {
			throw e;
		}
	}
}