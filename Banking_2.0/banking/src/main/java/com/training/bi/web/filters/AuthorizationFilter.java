package com.training.bi.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.training.bi.bo.constants.Constants;
import com.training.bi.dao.entitites.Account;

//filter for checking user's authorization
public class AuthorizationFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest r = (HttpServletRequest)request;
		String path = r.getRequestURI();

		//if path contains "/login" and "/registration" 
		//filter doesn't check session for user's data existing
		if (path.contains("/login") || path.contains("/registration")) {
			chain.doFilter(request, response);
		}

		else {
			//get user's data from session
			Account ac = (Account) r.getSession().getAttribute(Constants.EMAIL);

			if (ac == null) {
				r.getRequestDispatcher(Constants.LOGIN_JSP).forward(r, response);
			} 
			
			else {
				chain.doFilter(r, response);
			}
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {
	}
}