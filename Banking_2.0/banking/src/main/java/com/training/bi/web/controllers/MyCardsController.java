package com.training.bi.web.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bi.bo.constants.Constants;
import com.training.bi.bo.services.CardManager;
import com.training.bi.dao.entitites.Account;
import com.training.bi.dao.entitites.Card;

public class MyCardsController extends BasicController {
	private static final long serialVersionUID = 1L;
	private CardManager cardManager;

	public MyCardsController() {
		super();
		cardManager = new CardManager(this.factory);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//get account ID of current user from session
		int accountId = ((Account) req.getSession().getAttribute(Constants.EMAIL)).getId();
		
		try {
			//get all user cards
			List<Card> cards = cardManager.getAccountCards(accountId);
			
			req.setAttribute(Constants.CARDS_LIST, cards);
			req.getRequestDispatcher(Constants.CARD_JSP).forward(req, resp);
		}
		
		catch (Exception e) {
			req.setAttribute(Constants.COMMON_ERROR, "Some problem. Please, try again");
			req.getRequestDispatcher(Constants.CARD_JSP).forward(req, resp);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}