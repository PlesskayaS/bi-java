package com.training.bi.dao.entitites;

//class that contains all the fields of table Account
public class Account {
	private int id;
	private String fName;
	private String sName;
	private String country;
	private String email;
	private String password;

	public Account() {
	}

	public Account(String fName, String sName, String country, String email, String password) {
		this();
		this.fName = fName;
		this.sName = sName;
		this.country = country;
		this.email = email;
		this.password = password;
	}

	public Account(int id, String fName, String sName, String country, String email, String password) {
		this(fName, sName, country, email, password);
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int newId) {
		id = newId;
	}

	public String getFName() {
		return fName;
	}

	public void setFName(String newFName) {
		fName = newFName;
	}

	public String getSName() {
		return sName;
	}

	public void setSName(String newSName) {
		sName = newSName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String newCountry) {
		country = newCountry;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}
}