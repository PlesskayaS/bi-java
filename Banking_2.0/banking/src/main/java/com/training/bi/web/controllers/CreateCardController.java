package com.training.bi.web.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bi.bo.constants.Constants;
import com.training.bi.bo.services.CardManager;
import com.training.bi.dao.entitites.Account;

public class CreateCardController extends BasicController {
	private static final long serialVersionUID = 1L;
	private CardManager cardManager;

	public CreateCardController() {
		super();
		cardManager = new CardManager(this.factory);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//get account ID of current user from session
		int idAccount = ((Account) req.getSession().getAttribute(Constants.EMAIL)).getId();
		
		try {
			cardManager.createNewCard(idAccount);
			resp.sendRedirect(req.getContextPath() + "/my_cards");
			
		} catch (Exception e) {
			req.setAttribute(Constants.COMMON_ERROR, "Some problem. Please, try again");
			req.getRequestDispatcher("/my_cards").forward(req, resp);
		}
	}
}