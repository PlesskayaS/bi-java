package com.training.bi.bo.services;

import com.training.bi.dao.entitites.Account;
import com.training.bi.dao.interfaces.IAccountDAO;
import com.training.bi.dao.interfaces.IFactoryDAO;

//service for operations with account
public class UserManager {
	private IAccountDAO accountJdbc;

	public UserManager(IFactoryDAO factory) {
		this.accountJdbc = factory.createAccountJDBC();
	}

	public Account findUser(String email) throws Exception {
		return accountJdbc.getAccount(email.toLowerCase());
	}

	//return "true" if user is created and "false" if operation failed
	public boolean createUser(Account newAccount) throws Exception {

		if (newAccount.getFName().isEmpty() 
				|| newAccount.getSName().isEmpty() 
				|| newAccount.getPassword().isEmpty()
				|| newAccount.getEmail().isEmpty()) {
			return false;
		}

		try {
			accountJdbc.createAccount(newAccount);
		} 
		
		catch (Exception e) {
			throw e;
		}

		return true;
	}

	//return "true" if user exist and enter correct email and password
	public boolean autorization(String email, String password) {
		if (email.isEmpty() || password.isEmpty()) {
			return false;
		}

		Account account = null;
		try {
			account = findUser(email);
		} 
		
		catch (Exception e) {
			return false;
		}

		if (account == null) {
			return false;
		}

		return account.getPassword().equals(password);
	}
}