package com.training.bi.bo.constants;

public class Constants {
	
	//JSP pages paths
    public final static String LOGIN_JSP = "/WEB-INF/views/login.jsp";
    public final static String REGISTRATION_JSP = "/WEB-INF/views/registration.jsp";
    public final static String ERROR_JSP = "/WEB-INF/views/error.jsp";
    public final static String CARD_JSP = "/WEB-INF/views/my_cards.jsp";
    public final static String MY_TRANSACTION_JSP = "/WEB-INF/views/my_transactions.jsp";
    public final static String CREATE_TRANSACTION_JSP = "/WEB-INF/views/create_transaction.jsp";
    
    //Application constants
    public final static String EMAIL="email";
    public final static String PASSWORD="password";
    public final static String REPEAT_PASSWORD = "repeat_password";
    public final static int MIN_PASS_LENGHT = 6;
    public final static String F_NAME = "first_name";
    public final static String S_NAME = "second_name";
    public final static String COUNTRY = "country";
    public final static String ERROR_REG = "error_registration";
    public final static String ERROR_TRANSACT = "error_transaction";
    public final static String ERROR_LOG = "error_login";
    public final static double START_BALANCE = 0;
    public final static String COMMON_ERROR = "common_error";
    public final static String CARDS_LIST = "card_list";
    public final static String TRANSACT_LIST = "transaction_list";
    public final static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
    		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public final static String CARD_TO = "card_number_to";
    public final static String CARD_FROM = "card_number_from";
    public final static String SEND_SUM = "send_sum";
}