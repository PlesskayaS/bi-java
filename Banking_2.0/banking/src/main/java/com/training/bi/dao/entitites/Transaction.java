package com.training.bi.dao.entitites;

import java.sql.Date;

//class that contains all the fields of table Transaction
public class Transaction {
	private long id;
	private Date transactDate;
	private long cardFrom;
	private long cardTo;
	private double cashSum;

	public Transaction() {
	}

	public Transaction(long cardFrom, long cardTo, double cashSum) {
		this();
		this.cardFrom = cardFrom;
		this.cardTo = cardTo;
		this.cashSum = cashSum;
	}

	public Transaction(Date transactDate, long cardFrom, long cardTo, double cashSum) {
		this(cardFrom, cardTo, cashSum);
		this.transactDate = transactDate;
	}

	public Transaction(long id, Date transactDate, long cardFrom, long cardTo, double cashSum) {
		this(transactDate, cardFrom, cardTo, cashSum);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long newId) {
		id = newId;
	}

	public Date getDate() {
		return transactDate;
	}

	public void setTransactDate(Date newTtransactDate) {
		transactDate = newTtransactDate;
	}

	public long getCardFrom() {
		return cardFrom;
	}

	public void setCardFrom(long newCardFrom) {
		cardFrom = newCardFrom;
	}

	public long getCardTo() {
		return cardTo;
	}

	public void setCardTo(long newCardTo) {
		cardTo = newCardTo;
	}

	public double getCashSum() {
		return cashSum;
	}

	public void setId(double newCashSum) {
		cashSum = newCashSum;
	}
}