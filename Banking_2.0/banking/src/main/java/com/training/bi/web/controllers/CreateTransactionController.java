package com.training.bi.web.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bi.bo.constants.Constants;
import com.training.bi.bo.services.CardManager;
import com.training.bi.bo.services.TransactionManager;
import com.training.bi.dao.entitites.Account;

public class CreateTransactionController extends BasicController {
	private static final long serialVersionUID = 1L;
	private TransactionManager transactManager;
	private CardManager cardManager;

	public CreateTransactionController() {
		super();
		transactManager = new TransactionManager(this.factory);
		cardManager = new CardManager(this.factory);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//get card ID of current user from query string
		String cardFrom = req.getParameter(Constants.CARD_FROM);
		
		//get account ID of current user from session
		int accountId = ((Account) req.getSession().getAttribute(Constants.EMAIL)).getId();

		if (cardFrom.isEmpty()) {
			resp.sendRedirect(req.getContextPath() + "/my_cards");
		}

		else {
			try { 
				//try to convert value
				long realCard = Long.valueOf(cardFrom).longValue();
				//check the owner of the card
				boolean cardValid = cardManager.isCardValid(realCard, accountId);

				if (cardValid) {
					req.getSession().setAttribute(Constants.CARD_FROM, realCard);
					req.getRequestDispatcher(Constants.CREATE_TRANSACTION_JSP).forward(req, resp);
				}

				else {
					req.setAttribute(Constants.COMMON_ERROR, "Wrong card number");
					req.getRequestDispatcher("/my_cards").forward(req, resp);
				}

			} catch (Exception e) {
				req.setAttribute(Constants.COMMON_ERROR, "Wrong card number");
				req.getRequestDispatcher("/my_cards").forward(req, resp);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//get card number from session
		long cardFromId = (long) req.getSession().getAttribute(Constants.CARD_FROM);
		
		String reqCardTo = req.getParameter(Constants.CARD_TO).trim();
		String reqSendSum = req.getParameter(Constants.SEND_SUM).trim();

		try {
			//try to convert values
			long cardToId = Long.valueOf(reqCardTo).longValue();
			double sumMoney = Double.valueOf(reqSendSum).doubleValue();

			transactManager.createTransaction(cardFromId, cardToId, sumMoney);
			resp.sendRedirect(req.getContextPath() + "/my_transactions");
		}

		catch (NumberFormatException e) {
			req.setAttribute(Constants.ERROR_TRANSACT, "Wrong destination card number");
			req.getRequestDispatcher(Constants.CREATE_TRANSACTION_JSP).forward(req, resp);
		}

		catch (Exception e) {
			req.setAttribute(Constants.ERROR_TRANSACT, e.getMessage());
			req.getRequestDispatcher(Constants.CREATE_TRANSACTION_JSP).forward(req, resp);
		}
	}
}