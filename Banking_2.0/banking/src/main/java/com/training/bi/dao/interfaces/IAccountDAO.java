package com.training.bi.dao.interfaces;

import java.util.List;
import com.training.bi.dao.entitites.Account;

//Interface for objects from Account class with CRUD orepations
public interface IAccountDAO {
	void createAccount(Account newAccount) throws Exception;

	void deleteAccount(Account account) throws Exception;

	void updateAccount(Account account) throws Exception;

	Account getAccount(int accountId) throws Exception;

	Account getAccount(String email) throws Exception;

	List<Account> getAllAccount() throws Exception;
}