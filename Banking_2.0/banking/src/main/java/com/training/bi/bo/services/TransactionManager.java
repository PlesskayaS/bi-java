package com.training.bi.bo.services;

import java.util.ArrayList;
import java.util.List;

import com.training.bi.dao.entitites.Card;
import com.training.bi.dao.entitites.Transaction;
import com.training.bi.dao.interfaces.ICardDAO;
import com.training.bi.dao.interfaces.IFactoryDAO;
import com.training.bi.dao.interfaces.ITransactionDAO;

//service for operations with transactions
public class TransactionManager {
	private ITransactionDAO transactDao;
	private ICardDAO cardDao;

	public TransactionManager(IFactoryDAO factory) {
		this.transactDao = factory.createTransactionJDBC();
		this.cardDao = factory.createCardJDBC();
	}

	public List<Transaction> getUserTransactions(int accountId) throws Exception {
		List<Transaction> transactList = new ArrayList<Transaction>();
		
		//get all user's cards
		List<Card> allCardsList = this.getAllCards(accountId);

		if (!allCardsList.isEmpty()) {
			for (Card tempCardList : allCardsList) {

				long cardId = tempCardList.getId();
				
				//get all transactions of this card
				List<Transaction> cardsTransactions = this.getCardTransactions(cardId);

				if (!cardsTransactions.isEmpty()) {
					//adding all transactions to common collection
					transactList.addAll(cardsTransactions);
				}
			}
		}
		return transactList;
	}

	public void createTransaction(long cardFromId, long cardToId, double sumMoney) throws Exception {
		List<Card> cardsList = new ArrayList<Card> ();
		
		//get card destination
		Card cardTo = cardDao.getCard(cardToId);

		if (cardTo == null) {
			throw new Exception("Invalid destination card number.");
		}
		
		//get source card
		Card cardFrom = cardDao.getCard(cardFromId);

		if (cardFrom == null) {
			throw new Exception("Invalid source card number.");
		}

		if (sumMoney > cardFrom.getCash()) {
			throw new Exception("There is not enough money on the card.");
		}

		//subtracting money from cardFrom
		double subSum = cardFrom.getCash() - sumMoney;
		cardFrom.setCash(subSum);
		
		cardsList.add(cardFrom);
		
		//adding money to cardTo
		double addSum = cardTo.getCash() + sumMoney;
		cardTo.setCash(addSum);
		
		cardsList.add(cardTo);

		cardDao.updateCards(cardsList);

		//if previous method completed without any exception insert transaction
		Transaction newTransaction = new Transaction(cardFromId, cardToId, sumMoney);
		transactDao.createTransaction(newTransaction);
	}

	private List<Transaction> getCardTransactions(long cardId) throws Exception {
		return transactDao.getCardTransactions(cardId);
	}

	private List<Card> getAllCards(int accountId) throws Exception {
		return cardDao.getCardsOfAccountId(accountId);
	}
}