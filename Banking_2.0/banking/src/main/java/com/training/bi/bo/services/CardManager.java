package com.training.bi.bo.services;

import java.util.ArrayList;
import java.util.List;

import com.training.bi.bo.constants.Constants;
import com.training.bi.dao.entitites.Card;
import com.training.bi.dao.interfaces.ICardDAO;
import com.training.bi.dao.interfaces.IFactoryDAO;

//service for operations with cards
public class CardManager {
	private ICardDAO cardDao;

	public CardManager(IFactoryDAO factory) {
		cardDao = factory.createCardJDBC();
	}

	public List<Card> getAccountCards(int accountId) throws Exception {
		List<Card> accountCards = new ArrayList<Card>();
		accountCards = cardDao.getCardsOfAccountId(accountId);

		return accountCards;
	}

	public Card createNewCard(int idAccount) throws Exception {

		Card newCard = new Card(idAccount, Constants.START_BALANCE);
		cardDao.createCard(newCard);

		return newCard;
	}

	// card and owner validation for new transaction
	public boolean isCardValid(long cardId, int accountId) throws Exception {

		Card card = cardDao.getCard(cardId);

		if (card != null && card.getIdAccount() == accountId) {
			return true;
		}

		return false;
	}
}