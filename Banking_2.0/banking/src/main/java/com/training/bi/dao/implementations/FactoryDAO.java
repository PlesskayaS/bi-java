package com.training.bi.dao.implementations;

import com.training.bi.dao.ConnectionManager;
import com.training.bi.dao.interfaces.IAccountDAO;
import com.training.bi.dao.interfaces.ICardDAO;
import com.training.bi.dao.interfaces.IFactoryDAO;
import com.training.bi.dao.interfaces.ITransactionDAO;

public class FactoryDAO implements IFactoryDAO{

	private ConnectionManager connectionManager;

	public FactoryDAO(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public ICardDAO createCardJDBC() {		
		return new CardJdbcDAO(connectionManager);
	}

	@Override
	public ITransactionDAO createTransactionJDBC() {
		return new TransactionJdbcDAO(connectionManager);
	}

	@Override
	public IAccountDAO createAccountJDBC() {
		return new AccountJdbcDAO(connectionManager);
	}
}