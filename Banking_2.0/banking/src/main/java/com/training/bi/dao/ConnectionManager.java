package com.training.bi.dao;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

//class for provide connection to DB
public class ConnectionManager {
	private static volatile DataSource dataSource;
	
	//Singleton is used for creating DataSource object
	private static DataSource getDataSource() throws NamingException, SQLException {
		DataSource localDataSource = dataSource;
		
		if (localDataSource == null) {
			synchronized (ConnectionManager.class) {
	
					Context ctx = new InitialContext();
					
					//get data from context.xml for accessing DB
		            Context envContext = (Context) ctx.lookup("java:/comp/env");
		            localDataSource = (DataSource) envContext.lookup("jdbc/bankingDB");
		            dataSource = localDataSource;
			}
		}
		return localDataSource;
	}

	public Connection getConnection() throws SQLException, NamingException {
		return getDataSource().getConnection();
	}
}