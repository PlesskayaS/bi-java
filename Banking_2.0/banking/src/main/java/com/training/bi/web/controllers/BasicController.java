package com.training.bi.web.controllers;

import javax.servlet.http.HttpServlet;

import com.training.bi.dao.ConnectionManager;
import com.training.bi.dao.implementations.FactoryDAO;
import com.training.bi.dao.interfaces.IFactoryDAO;

public class BasicController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected IFactoryDAO factory;
	
	public BasicController() {
		ConnectionManager connectionManager = new ConnectionManager();
		factory = new FactoryDAO(connectionManager);
	}
}