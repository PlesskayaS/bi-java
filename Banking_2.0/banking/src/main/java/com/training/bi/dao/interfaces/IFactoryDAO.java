package com.training.bi.dao.interfaces;

//interface factory with declared methods for creating data access objects of interfaces
public interface IFactoryDAO {

	ICardDAO createCardJDBC();

	ITransactionDAO createTransactionJDBC();

	IAccountDAO createAccountJDBC();
}