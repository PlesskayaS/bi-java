package com.training.bi.web.controllers;

import java.io.IOException;

import com.training.bi.bo.constants.Constants;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bi.bo.services.UserManager;
import com.training.bi.dao.entitites.Account;

public class LoginController extends BasicController {
	private static final long serialVersionUID = 1L;
	private UserManager userService;

	public LoginController() {
		super();
		userService = new UserManager(this.factory);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(Constants.LOGIN_JSP).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String email = req.getParameter(Constants.EMAIL).trim();
		String password = req.getParameter(Constants.PASSWORD).trim();

		//check user credentials
		boolean isAutorized = userService.autorization(email, password);

		if (isAutorized == true) {
			try {
				Account userAccount = userService.findUser(email);

				// put authorized user into session
				req.getSession().setAttribute(Constants.EMAIL, userAccount);
				resp.sendRedirect(req.getContextPath() + "/my_cards");
				
			} catch (Exception e) {
				req.getRequestDispatcher(Constants.ERROR_JSP).forward(req, resp);
			}

		} else {
			req.setAttribute(Constants.ERROR_LOG, "Email or password isn't correct");
			req.getRequestDispatcher(Constants.LOGIN_JSP).forward(req, resp);
		}
	}
}