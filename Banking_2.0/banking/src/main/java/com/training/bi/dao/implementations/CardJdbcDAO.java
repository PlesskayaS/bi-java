package com.training.bi.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.training.bi.dao.ConnectionManager;
import com.training.bi.dao.entitites.Card;
import com.training.bi.dao.interfaces.ICardDAO;

//Implementation for interface ICardDAO for accessing table "CREDIT_CARD" in DB
public class CardJdbcDAO implements ICardDAO {

	private ConnectionManager connectionManager;

	public CardJdbcDAO(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public void createCard(Card newCard) throws Exception {
		// this array contains the name of the column in the
		// "CREDIT_CARD" table that contain the auto-generated keys that should be returned.
		String generatedColumns[] = { "CREDIT_CARD_ID" };
		String insertCard = "INSERT INTO CREDIT_CARD (CREDIT_CARD_ID, ACCOUNT_ID, CASH_AMOUNT) VALUES (CARD_ID_SEQ.NEXTVAL, ?, ?)";
		
		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(insertCard, generatedColumns)) {

			statement.setInt(1, newCard.getIdAccount());
			statement.setDouble(2, newCard.getCash());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Creating card failed, no rows affected.");
			}

			// getting auto-generated id
			ResultSet generatedKey = statement.getGeneratedKeys();
			while (generatedKey.next()) {
				int id = generatedKey.getInt(1);
				newCard.setId(id); // setting auto-generated id in card
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void updateCard(Card card) throws Exception {
		String updateCard = "UPDATE CREDIT_CARD SET ACCOUNT_ID = ?, CASH_AMOUNT = ? WHERE CREDIT_CARD_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(updateCard)) {

			statement.setInt(1, card.getIdAccount());
			statement.setDouble(2, card.getCash());
			statement.setLong(3, card.getId());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Update is failed, no rows affected.");
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void updateCards(List<Card> cards) throws Exception {	
		Connection dbConnection = null;
		PreparedStatement statementFrom = null;
		PreparedStatement statementTo = null;

		String updateCard = "UPDATE CREDIT_CARD SET CASH_AMOUNT = ? WHERE CREDIT_CARD_ID = ?";
		
		try {
			dbConnection = connectionManager.getConnection();
			dbConnection.setAutoCommit(false); //transaction block start
			
			for(Card card: cards)
			{
				statementFrom = dbConnection.prepareStatement(updateCard);

				statementFrom.setDouble(1, card.getCash());
				statementFrom.setLong(2, card.getId());

				int affectedRows = statementFrom.executeUpdate();

				if (affectedRows == 0) {
					throw new Exception("Transaction failed.");
				}
			}

			dbConnection.commit(); //transaction block end. complete transaction
		}

		catch (Exception e) {
			dbConnection.rollback(); //rollback all changes if catch any exception
			throw e;
		}

		finally {

			if (statementFrom != null) {
				statementFrom.close();
			}

			if (statementTo != null) {
				statementTo.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	@Override
	public void deleteCard(Card card) throws Exception {
		String deleteCard = "DELETE FROM CREDIT_CARD WHERE CREDIT_CARD_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(deleteCard)) {

			statement.setLong(1, card.getId());

			int affectedRows = statement.executeUpdate();

			if (affectedRows == 0) {
				throw new Exception("Delete is failed, no rows affected.");
			}
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Card getCard(long id) throws Exception {
		String getCard = "SELECT * FROM CREDIT_CARD WHERE CREDIT_CARD_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getCard)) {
			statement.setLong(1, id);

			ResultSet result = statement.executeQuery();

			Card card = null;

			while (result.next()) {
				long creditNum = result.getLong("CREDIT_CARD_ID");
				int accountNum = result.getInt("ACCOUNT_ID");
				double sumMoney = result.getDouble("CASH_AMOUNT");

				card = new Card(creditNum, accountNum, sumMoney);
			}
			return card;
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Card> getCardsOfAccountId(int accountId) throws Exception {
		List<Card> allAccountCards = new ArrayList<Card>();
		String getCardsOfAccountId = "SELECT * FROM CREDIT_CARD WHERE ACCOUNT_ID = ?";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getCardsOfAccountId)) {
			statement.setInt(1, accountId);

			ResultSet result = statement.executeQuery();

			while (result.next()) {
				long creditNum = result.getLong("CREDIT_CARD_ID");
				int accountNum = result.getInt("ACCOUNT_ID");
				double sumMoney = result.getDouble("CASH_AMOUNT");

				allAccountCards.add(new Card(creditNum, accountNum, sumMoney));
			}
			return allAccountCards;
		}

		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Card> getAllCard() throws Exception {
		List<Card> allCard = new ArrayList<Card>();
		String getAllCard = "SELECT * FROM CREDIT_CARD";

		try (Connection dbConnection = connectionManager.getConnection();
				PreparedStatement statement = dbConnection.prepareStatement(getAllCard)) {

			ResultSet result = statement.executeQuery();

			while (result.next()) {
				long creditNum = result.getLong("CREDIT_CARD_ID");
				int accountNum = result.getInt("ACCOUNT_ID");
				double sumMoney = result.getDouble("CASH_AMOUNT");

				Card card = new Card(creditNum, accountNum, sumMoney);
				allCard.add(card);
			}

			return allCard;
		}

		catch (Exception e) {
			throw e;
		}
	}
}