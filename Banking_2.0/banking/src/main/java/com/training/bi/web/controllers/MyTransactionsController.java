package com.training.bi.web.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.bi.bo.constants.Constants;
import com.training.bi.bo.services.TransactionManager;
import com.training.bi.dao.entitites.Account;
import com.training.bi.dao.entitites.Transaction;

public class MyTransactionsController extends BasicController {
	
	private static final long serialVersionUID = 1L;
	private TransactionManager transactManager;

	public MyTransactionsController() {
		super();
		transactManager = new TransactionManager(this.factory);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int accId = ((Account) req.getSession().getAttribute(Constants.EMAIL)).getId();
		try {
			List<Transaction> list = transactManager.getUserTransactions(accId);
			
			req.setAttribute(Constants.TRANSACT_LIST, list);
			req.getRequestDispatcher(Constants.MY_TRANSACTION_JSP).forward(req, resp);
			
		} catch (Exception e) {
			req.getRequestDispatcher(Constants.ERROR_JSP).forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
