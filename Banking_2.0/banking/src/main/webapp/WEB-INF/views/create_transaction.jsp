<%@ page import="com.training.bi.bo.constants.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>New transaction</title>
</head>
<body>
	<p style="color: red;"><%=request.getAttribute(Constants.ERROR_TRANSACT) == null ? ""
					: request.getAttribute(Constants.ERROR_TRANSACT).toString()%></p>

	<form name="transactionForm" action="<%=request.getContextPath() + "/create_transaction"%>" method="post">
		<p>Enter card number you want to send money:</p>
		<input name="<%=Constants.CARD_TO%>" type="number" min="1" step="any">
		<p>Enter sum of money for sending:</p>
		<input name="<%=Constants.SEND_SUM%>" type="number" min="1" step="any">
		<p>
			<input type="submit" value="Create transaction">
		</p>
	</form>
	<p>
		<a href=<%=request.getContextPath() + "/my_cards"%>>Cancel transaction. Back to my cards</a>
	</p>
	<footer> 
		<p> <a href=<%=request.getContextPath() + "/logout"%>>Logout</a> </p>
		<p>Developed by Svetlana Plesskaya</p>
	</footer>
</body>
</html>