<%@ page import="com.training.bi.bo.constants.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login</title>
</head>
<body>
	<p style="color: red;"><%=request.getAttribute(Constants.ERROR_LOG) == null ? ""
					: request.getAttribute(Constants.ERROR_LOG).toString()%>
	</p>
	<form name="loginForm" action="<%=request.getContextPath() + "/login"%>" method="post">
		<p>Enter email:</p>
		<input name="<%=Constants.EMAIL%>" type="text">
		<p>Enter password:</p>
		<input name="<%=Constants.PASSWORD%>" type="password">
		<p>
			<input type="submit" value="Login">
		</p>
	</form>
	<p>
		<a href=<%=request.getContextPath() + "/registration"%>>Registrate new account</a>
	</p>
	<footer>
		<p>Developed by Svetlana Plesskaya</p>
	</footer>
</body>
</html>