<%@ page import="com.training.bi.bo.constants.Constants"%>
<%@ page import="java.util.List"%>
<%@ page import="com.training.bi.dao.entitites.*"%>
<%@ page import="java.sql.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>My transactions</title>
</head>
<body>
	<h1>
		Hello, <%=((Account) request.getSession().getAttribute(Constants.EMAIL)).getFName()%>
	</h1>
	<%
		List<Transaction> transactList = (List<Transaction>) request.getAttribute(Constants.TRANSACT_LIST);

		if (transactList == null || transactList.isEmpty()) {
	%>
	
	<a href=<%=request.getContextPath() + "/my_cards"%>>Create first transaction</a>

	<%
		} else {
	%>
	<table cellspacing="20">
		<tr>
			<th>Transaction number</th>
			<th>Transaction date</th>
			<th>Card number from</th>
			<th>Card number to</th>
			<th>Sum of money</th>
			<th></th>
		</tr>
		<%
			for (Transaction transact : transactList) {
		%>
		<tr>
			<td><%=transact.getId()%></td>
			<td><%=(new SimpleDateFormat("YY.MM.dd")).format(transact.getDate())%></td>
			<td><%=transact.getCardFrom()%></td>
			<td><%=transact.getCardTo()%></td>
			<td><%=transact.getCashSum()%></td>
			<%
				}
			%>
	</table>
	<p>
		<a href=<%=request.getContextPath() + "/my_cards"%>>Back to my cards</a>
	</p>
	<%
		}
	%>
	<p>
		<a href=<%=request.getContextPath() + "/logout"%>>Logout</a>
	</p>
	<footer>
		<p>Developed by Svetlana Plesskaya</p>
	</footer>
</body>
</html>