<%@ page import="com.training.bi.bo.constants.Constants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Registration</title>
</head>
<body>
	<p style="color: red;"><%=request.getAttribute(Constants.ERROR_REG) == null ? ""
					: request.getAttribute(Constants.ERROR_REG).toString()%>
	</p>
	<form name="registrationForm"
		action="<%=request.getContextPath() + "/registration"%>" method="post">
		<p>Enter Email:</p>
		<input type="text" name="<%=Constants.EMAIL%>">
		<p>Enter first name:</p>
		<input type="text" name="<%=Constants.F_NAME%>">
		<p>Enter second name:</p>
		<input type="text" name="<%=Constants.S_NAME%>">
		<p>Enter your country:</p>
		<input type="text" name="<%=Constants.COUNTRY%>">
		<p>Enter password:</p>
		<input type="password" name="<%=Constants.PASSWORD%>">
		<p>Confirm password</p>
		<input type="password" name="<%=Constants.REPEAT_PASSWORD%>">
		<p>
			<input type="submit" value="Create account">
		</p>
	</form>
	<p>
		<a href=<%=request.getContextPath() + "/login"%>>Cancel registration</a>
	</p>
	<footer> 
		<p>Developed by Svetlana Plesskaya</p>
	</footer>
</body>
</html>