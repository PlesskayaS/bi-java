<%@ page import="com.training.bi.bo.constants.Constants"%>
<%@ page import="java.util.List"%>
<%@ page import="com.training.bi.dao.entitites.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>My cards</title>
</head>
<body>
	<h1> 
		Hello, <%=((Account) request.getSession().getAttribute(Constants.EMAIL)).getFName()%>
	</h1>
	<p style="color: red;"><%=request.getAttribute(Constants.COMMON_ERROR) == null ? ""
					: request.getAttribute(Constants.COMMON_ERROR).toString()%>
	</p>
	<%
		List<Card> cardList = (List<Card>) request.getAttribute(Constants.CARDS_LIST);

		if (cardList == null || cardList.isEmpty()) {
	%>
	
	<a href=<%=request.getContextPath() + "/create_card"%>>Create first card</a>
	
	<%
		} else {
	%>
	<table cellspacing="15">
		<tr>
			<th>Card number</th>
			<th>Sum of money</th>
			<th></th>
		</tr>
		<%
			for (Card card : cardList) {
		%>
		<tr>
			<td><%=card.getId()%></td>
			<td><%=card.getCash()%></td>
			<td><a href=<%=request.getContextPath() + "/create_transaction?" 
			+ Constants.CARD_FROM + "=" + card.getId()%>>Send money</a></td>
		</tr>
		<%
			}
		%>
	</table>
	<p>
		<a href=<%=request.getContextPath() + "/create_card"%>>Add new card</a>
	</p>
	<p>
		<a href=<%=request.getContextPath() + "/my_transactions"%>>Check my transactions</a>
	</p>
	<%
		}
	%>
	<p>
		<a href=<%=request.getContextPath() + "/logout"%>>Logout</a>
	</p>
	<footer>
		<p>Developed by Svetlana Plesskaya</p>
	</footer>
</body>
</html>